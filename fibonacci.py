"""
@file fibonacci.py
@brief An intreractive Fibonacci number calculator
@details Repeatedly prompts the user for indices of desired fibonacci numbers and prints them to the commandline\n\n
Source code can be found at https://bitbucket.org/reluttre/lab1/src/master/

@author Robert Luttrell
@date 01/21/2021
"""

def fib(idx):
    """
    @brief      This function calculates a Fibonacci number at a specific index.
    @param idx  An integer specifying the index of the desired Fibonacci number.
    """

    # Dynamic programming approach:
    # 1. Def: F[i] = the fibonacci number at the i'th index
    # 2. BC's: F[0] = 0
    #          F[1] = 1
    # 3. Sol'n: F[idx]
    # 4. Formula: F[i] = f[i-2] + f[i-1]

    F = [0] * (idx + 1)

    if idx == 0 or idx == 1:
        return idx

    F[0] = 0
    F[1] = 1

    for i in range(2, idx + 1):
        F[i] = F[i - 2] + F[i - 1]

    return F[idx]

def is_int(in_string):
    """
    @brief              Returns True if in_string can be represented as an int, else False.
    @param in_string    String to check if int
    """
    try:
        int(in_string)
        return True
    except ValueError:
        return False

def is_valid_index(idx_string):
    """
    @brief              Returns True if idx_string is a valid fibonacci index, else False.
    @param idx_string   A string representing a fibonacci index.
    """
    return is_int(idx_string)  and int(idx_string) >= 0

def run_program():
    """
    @brief  Repeatedly prompts user for fibonacci index to calculate until interrupt.
    """

    print("\nWelcome to the Fibonacci Enumerator. Press Ctrl+C at any time to exit.\n")

    while True:
        print("Please enter an index to receive a Fibonacci number!")
        idx_string = input("    idx: ")
        if is_valid_index(idx_string):
            print("\nFibonacci number at index {:} is {:}.\n".format(idx_string, fib(int(idx_string))))
        else:
            print("Invalid index. Index must be a positive integer.\n")

if __name__ == "__main__":
    run_program()
